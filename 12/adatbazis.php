<?php
//Adatbázis csatlakozás
//host
$dbHost = 'localhost';
//user
$dbUser = 'root';
//password
$dbPassword = '';
//dbname
$dbName = 'classicmodels';

//csatlakozás vagy állj
$link = @mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName) or die('Hiba az adatbázis kapcsolatban...'.mysqli_connect_error());

//lekérés összeállítása
$qry = "SELECT * FROM employees";

//lekérés futtatása
$result = mysqli_query($link, $qry) or die(mysqli_error($link));

//kapott eredmény feldolgozása
//1 sor kibontása
$row = mysqli_fetch_row($result);
echo '<pre>'.var_export($row,true).'</pre>';
//1 sor kibontása
$row = mysqli_fetch_assoc($result);
echo '<pre>'.var_export($row,true).'</pre>';
//1 sor kibontása
$row = mysqli_fetch_array($result);
echo '<pre>'.var_export($row,true).'</pre>';

//1 sor kibontása
$row = mysqli_fetch_object($result);
echo '<pre>'.var_export($row,true).'</pre>';

/*//többi kibontása 1 lépésben
$rows = mysqli_fetch_all($result, MYSQLI_ASSOC );
echo '<pre>'.var_export($rows,true).'</pre>';*/

$employeeList = '<ul>';//lista nyitása
//a többi kibontása ciklusban
while( $row = mysqli_fetch_assoc($result) ){
    //elemek belefűzése a listába
    $employeeList .= "<li>
                        <b>{$row['employeeNumber']} </b>
                        {$row['firstName']}
                        {$row['lastName']}
                        - Phone: {$row['extension']}
                      </li>";
    //echo '<pre>'.var_export($row,true).'</pre>';
}
//lista zárása
$employeeList .= '</ul>';

//kiírás 1 lépésben
echo $employeeList;

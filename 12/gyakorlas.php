<?php
require "connect.php";/** @var mysqli $link  - db csatlakozás betöltése */

/**
 * @todo Órai feladat: Készíts egy táblázatot! (vagy listát) és írd ki, amiben az irodák
 * azonosítói,Országnév,városnév, alkalmazottak száma
 * -csatlakozás
 * -lekérés megírása
 * -lekérés futtatása
 * -eredmények feldolgozása (table összeállítása változóba)
 * -table kiírása
 * @todo HF:
 * +16,17,18
 * 16. összes forgalom

17. 2004 évi megrendelések összege

18. 2004 évi nyereség eladott termékek összege-eladott termékek beszerzési ára
 */

//16. összes forgalom
$qry = "SELECT SUM(priceeach*quantityordered) total 
        FROM orders o
        LEFT JOIN orderdetails od
        ON o.ordernumber = od.ordernumber
        WHERE 
              status IN('shipped'/*,'resolved'*/) ";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_row($result);
$output = "<div>Az összes forgalom: USD ".number_format($row[0], 2,",",   " ")."</div>";
echo $output;

//
//17. 2004 évi megrendelések összege
//
$qry = "SELECT SUM(priceeach*quantityordered) total 
        FROM orders o
        LEFT JOIN orderdetails od
        ON o.ordernumber = od.ordernumber
        WHERE 
              status = 'shipped' AND YEAR(orderdate) = 2004  ";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_row($result);
$output = "<div>Az összes megrendelés összege (2004): USD ".number_format($row[0], 2,",",   " ")."</div>";
echo $output;

//18. 2004 évi nyereség
$qry = "SELECT SUM((priceeach-buyprice)*quantityordered) total 
        FROM orders o
        LEFT JOIN orderdetails od
        ON o.ordernumber = od.ordernumber
        LEFT JOIN products p 
        ON od.productcode = p.productcode
        WHERE 
              status = 'shipped' AND YEAR(orderdate) = 2004  ";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_row($result);
$output = "<div>A 2004 évi nyereség: USD ".number_format($row[0], 2,",",   " ")."</div>";
echo $output;


//15.	listázzuk ki az amerikai (az iroda van ott) megrendelőket: (customers) név, város, ország
//ciklus
$qry = "SELECT 
            customername,
            c.country,
            c.city
        FROM offices o 
        LEFT JOIN employees e
        ON o.officecode = e.officecode
        LEFT JOIN customers c
        ON employeenumber = salesrepemployeenumber
        WHERE 
              o.country = 'USA' ";

$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$num = mysqli_num_rows($result);//ennyi találat van
//var_dump($num);
/** @todo HF: pontos válasz hogy miért vannak null rekordjaink, és javítani a lekérésen hogy ne legyenek */
$output = 'Találatok száma: '.$num;
 $output .= '<table>
            <tr>
             <th>nr.</th>
             <th>Vevő neve</th>
             <th>Ország</th>
             <th>Város</th>
            </tr>';
$i = 1;
while($row = mysqli_fetch_assoc($result)){
    //var_dump($row);
    if($row['customername'] !== NULL ) {
        $output .= "<tr>
                     <td>".$i++."</td>
                     <td>{$row['customername']}</td>
                     <td>{$row['country']}</td>
                     <td>{$row['city']}</td>
                    </tr>";

    }
}

$output .='</table>';
echo $output;

<?php
/**
 * Insert segéd
 * @param string $table
 * @param array $data
 * @return string
 */
function getInsertQuery(string $table, array $data = [] ,  ) :string
{
    $ret =  '';
    if($table !== '' && !empty($data)) {
        $ret .= "INSERT INTO `$table`(`".(implode('`,`',array_keys($data)))."`) VALUES ('".(implode("','",$data))."')";

    }
    return $ret;

}
/**
 * Update query segéd
 * @param string $table
 * @param int $id
 * @param array $data
 * @return string
 */
function getUpdateQuery(string $table, int $id , array $data = []) :string
{
    $ret = '';
    $elem = [];
    if($table !== '' && !empty($data)) {
        $ret .= "UPDATE `$table` SET ";
        foreach ($data as $key => $value){
            $elem[] = "`$key` = '$value'";
        }
        //var_dump($elem);
        $ret .= implode(',',$elem);
        $ret .= " WHERE id = $id LIMIT 1";
    }
    return $ret;
}
/**
 * text input mezők value értékének kiolvasása súlyozással db adattömbbel
 * @param string $fieldName
 * @param array $row
 * @return string
 */
function getInputValue(string $fieldName, array $row = []): string
{
    if (filter_input(INPUT_POST, $fieldName) !== null) {
        return filter_input(INPUT_POST, $fieldName);
    }
    if (array_key_exists($fieldName, $row)) {
        return $row[$fieldName];
    }
    return '';
}

<?php
//Erőforrások
require 'connect.php'; /** @var mysqli $link - adatbazási csatlakozás betöltése */
//post hibakezelés
if (!empty($_POST)) {

    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//hibák gyűjtő halmaza

    //név legyen min 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Hibás adat! Minimum 3 karakter!</span>';
    }

    //email legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e?
        $qry = "SELECT id FROM users WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        var_dump($row);
        if ($row !== null) {
            $errors['email'] = '<span class="error">Már foglalt email!</span>';
        }

    }

    //Jelszavak ellenőrzése min 6 karakter és egyezzenek
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');

    if (mb_strlen($password, 'utf-8') < 6) {
        $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
    } elseif ($password !== $repassword) {
        $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';
    } else {
        //nincs hiba a jelszó mezőn
        //jelszó elkódolása
        /* $secret_key = 'S3cR3T_k3Y!';
         for($i=1;$i<20;$i++){
             $password = md5($password);
         }*/
        $password = password_hash($password, PASSWORD_BCRYPT);
    }

    /** @todo: status checkbox az űrlapra */
    $status = filter_input(INPUT_POST, 'status') ?? 0;

    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status,
            'time_created' => date('Y-m-d H:i:s')
        ];
        //Adatbázisban tároljuk
        //kellene egy azonosító
        /**
         * @todo HF: tervezz egy users nevű adattáblát az adatok tárolása
         * mezőnév,tipus és a járulékos beállításaik
         * ********************************************************
         * id                int(11) unsigned auto_increment primary_key
         * name            varchar(255) utf8...
         * email            varchar(255) utf8... unique
         * password        varchar(80)
         * status            tinyint(1) unsigned default:0
         * (privilege)
         * time_created    datetime    default:date()
         * time_updated    datetime    null;
         * **********************************************************
         * uj user:
         * INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `time_created`, `time_updated`) VALUES (NULL, 'superadmin', 'hgy@iworkshop.hu', '123456', '1', '2022-04-12 17:51:25', NULL);
         */
        echo '<pre>' . var_export($data, true) . '</pre>';
        $qry = "INSERT INTO 
                `users`( 
                        `name`, 
                        `email`, 
                        `password`, 
                        `status`, 
                        `time_created`
                        ) 
                VALUES (
                        '{$data['name']}', 
                        '{$data['email']}', 
                        '{$data['password']}', 
                        '{$data['status']}', 
                        '{$data['time_created']}'
                        )";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //átirányítás
        header('location:' . $_SERVER['PHP_SELF']);
        exit();

    }

}



?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Regisztráció</title>
    <style>
        label {
            display: flex;
            flex-direction: column;
            margin-bottom: 1em;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<h1>Regisztráció</h1>
<!--PHP űrlap kiírása-->
<?php echo $form; ?>
</body>
</html>

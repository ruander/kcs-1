<?php
//erőforrások
$validFileTypes = ['text/plain','image/jpeg'];
//mappa létrehozása ahova majd tárolni szeretnénk a feltöltött file-okat
$dir='uploads/';
if(!is_dir($dir)){
    mkdir($dir, 0755, true);
}

if (!empty($_POST)) {
    $errors = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //filefeltöltés
    echo '<pre>' . var_export($_FILES['myFile'], true) . '</pre>';
    $fileUploaded = false;
    if($_FILES['myFile']['error'] !== 0){
        $errors['myFile'] = '<span class="error">Kötelező mező!</span>';
    }else{
        //van feltöltött file
        $fileUploaded = true;
        $fileType = mime_content_type($_FILES['myFile']['tmp_name']);
        if( !in_array($fileType , $validFileTypes) ){
            $errors['myFile'] = '<span class="error">Nem megendedett filetípus!</span>';
        }
    }
    if (empty($errors)) {
        //....

        if($fileUploaded){ //van feltöltött file
            var_dump(pathinfo($_FILES['myFile']['name']));
            //eredeti néven tároljuk el egy uploads mappába
            $fileName = $_FILES['myFile']['name'];
            if(is_uploaded_file($_FILES['myFile']['tmp_name'])){
                $success = move_uploaded_file($_FILES['myFile']['tmp_name'], $dir.$fileName);
                if($success){
                    echo 'sikeres filefeltöltés: '.$dir.$fileName;
                }else{
                    die('File feltöltési hiba');
                }
            }else{
                die('File feltöltési hiba');
            }
            //.....

        }

    }

}
?>
<form method="post" enctype="multipart/form-data">
    <label>
        <span>File feltöltése<sup>*</sup>:</span>
        <input type="file" name="myFile">
        <?php echo $errors['myFile'] ?? '' ?>
    </label>
    <input type="hidden" name="test" value="testvalue">
    <button>mehet</button>
    <!--<input type="submit" name="submit" value="Feltölt">-->
</form>

<?php
//Erőforrások
$validImageTypes = ['image/jpg', 'image/jpeg'];
$dir = 'images/';
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}

$imageFile = 'fekvo.jpg';

$info = getimagesize($imageFile);

if ($info) {
    //most csak jpeg tipusu képeket kezelünk
    //tipus ellenőrzése
    if (!in_array($info['mime'], $validImageTypes)) {
        die('Nem jó képtipus');
    }

    //méret ellenőrzése most egyik mérete sem lehet kisebb mint 100px
    $minWidth = $minHeight = 150;//kisebbik mérete legyen min ennyi
    /** @todo: HF : ha nem jó a méret, die */

    /** @todo HF: ha a kép kisebb mint 1200px akkor maradjon az eredeti méret (sima copy) */
    $imageMaxSize = 1200;
    $thumbWidth = $thumbHeight = 150;
    $originalWidth = $info[0];
    $originalHeight = $info[1];
    $ratio = $originalWidth / $originalHeight;
    $x = 0;//eredeti kép koordináta x
    $y = 0;//eredeti kép koordináta y
    /** álló kép esetében a magasság legyen max 1200, azaz a nagyobbik méret legyen 1200px mindig */
    //1200px szélességű képet szeretnénk a szerveren látni
    if($ratio > 1){//fekvő
        $targetWidth = $imageMaxSize;
        $targetHeight = round($targetWidth / $ratio);

        $targetThumbHeight = $thumbHeight;
        $targetThumbWidth = $targetThumbHeight * $ratio;

        $targetY = 0;
        $targetX = round(($thumbWidth - $targetThumbWidth) / 2);
    }else{//álló v négyzet
        $targetHeight = $imageMaxSize;
        $targetWidth = round($targetHeight * $ratio);

        $targetThumbWidth = $thumbWidth;
        $targetThumbHeight = $targetThumbWidth / $ratio;

        $targetX = 0;
        $targetY = round(($thumbHeight - $targetThumbHeight) / 2);
    }
    //eredeti kép memóriába
    $image = imagecreatefromjpeg($imageFile);

    //méretarányos kicsinyítés
    $canvas = imagecreatetruecolor($targetWidth, $targetHeight);
    imagecopyresampled($canvas, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $originalWidth, $originalHeight);

    //ez a file látszódjon képnek
    //header('Content-Type: image/jpeg');
    imagejpeg($canvas, $dir . 'uploadtest.jpg', 100);

    //IMAGECROP elkészítése

    $thumbCanvas = imagecreatetruecolor($thumbWidth, $thumbHeight);
    imagecopyresampled($thumbCanvas, $image, $targetX, $targetY, $x, $y, $targetThumbWidth, $targetThumbHeight, $originalWidth, $originalHeight);
    //header('Content-Type: image/jpeg');//látszódjon képnek a file
    imagejpeg($thumbCanvas, $dir.'uploadtest-thumb.jpg', 70);

    //takarítás
    imagedestroy($canvas);
    imagedestroy($image);
    imagedestroy($thumbCanvas);


    echo '<pre>' . var_export($info, true) . '</pre>';
    echo '<pre>' . var_export($ratio, true) . '</pre>';
}





























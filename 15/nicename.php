<?php
$badFileName = 'árvíz_tűrő tükör!/=fúrógép%-.jpg';//arvizturo-tukorfurogep.jpg

echo formatFileName($badFileName);
echo slug($badFileName);
/*
 * alakitsuk kisbetűsre
kicserélni az ékezetest nem ékezetesre:  á -> a
spaceket  -> -
speciális karakterek -> -
 */

/**
 * Filenév átalakító ékezet és speciális karakter mentesre
 * @param $fileName
 * @return array|string|string[]
 */
function formatFileName($fileName)
{
    $charsFrom = ['á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'Ä', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű', ' '];
    $charsTo = ['a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-'];

    $string = strtolower($fileName);
    $string = str_replace($charsFrom, $charsTo, $string);
    $string = preg_replace("/[^A-Za-z0-9_.]/", '-', $string );
    $string = str_replace(['--'], '-', $string);

    return $string;
}

/**
 * string to url-part
 * szöveg átalakító ékezet és speciális karakter mentesre
 * @param $fileName
 * @return array|string|string[]
 */
function slug($fileName)
{
    $charsFrom = ['á', 'ä', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', ' '];
    $charsTo = ['a', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', '-'];

    $string = mb_strtolower($fileName,'utf-8');
    $string = str_replace($charsFrom, $charsTo, $string);
    $string = preg_replace("/[^a-z0-9]/", '-', $string );
    $string = str_replace('--', '-', $string);

    return $string;
}

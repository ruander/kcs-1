<?php
//Tiszta PHP (pure PHP) file
//$ -> változó (variable)
//PRIMITIVEK
$name = "Horváth György";//operátor: = ->assign operator, értékadó |tipus: string
$price = 1499.50;//tipus : floating point number (float) lebegő pontos szám
$id = 12;//tipus: integer (int) - egész szám
$isMichaelJacksonAlive = false;//tipus: boolean (bool) - logikai igaz hamis

//fejlesztés közben(!) információ szerzés változókról és értékükről
echo '<pre>';
var_dump($name, $price, $id, $isMichaelJacksonAlive);
echo '</pre>';

$now = date("Y-m-d H:i:s");

//Írjuk ki: az oldal betöltésének dátumideje: YYYY-MM-DD HH:MM:SS
echo '<br>az oldal betöltésének dátumideje: ' . $now;//Operátor: . konkatenáció
echo "<br>Pontos idő: " . $now;

echo '<br>az oldal betöltésének dátumideje: $now';
echo "<br>Pontos idő: {$now}";

echo '<br>az oldal\'s betöltésének dátumideje: \$now';
echo "<br>\"helo\" Pontos idő: \$now";//operátor: \ -> escape operátor

////////műveletek
$a = 4;
$b = rand(0,2);
$c = rand(2, 8);
echo '<pre>';
var_dump($a, $b, $c , null);
echo '</pre>';

$sum = $a + $b + $c;
echo "<br>Az összegük: $sum";

$subtract = $a - $b;
echo "<br> $a - $b = $subtract";

$mtpl = $a * $b * $c;
echo "<br>szorzatuk : $mtpl";

//mivel $b változó, lehet az értéke 0, amivel viszont nem oszthatunk, ezért csak akkor végezzük el az osztást ha az értelmezhető
/*
if(condition){
    //igaz ág
    ...
}else{
    //hamis ág
    ...
}
 */

if($b != 0){
    // Operátor : (érétkvizsgálat) a == b egyenlőség vizsgálat, a!=b egyenlőtlenség vizsgálat;
    // a === b, a!==b -> érték és TIPUS vizsgálat , tehát  1 == '1'(igaz) de 1 === '1'(hamis)
    $x = $a / $b;
    echo "<br>$a / $b = $x";
}else{
    echo '<br>Az osztás nem értelmezhető mert b=0';
}



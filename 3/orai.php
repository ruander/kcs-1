<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>PHP alapok - változók, állandók, clean-coding</title>
    <style>
        pre {
            background: lightblue;
            border: 2px solid grey;
            padding: 1em;
        }
    </style>
</head>
<body>
<h1>Kód elhelyezése weboldalon</h1>
<p>Ez egy minta php kód, kiírja az idézőjel közötti szöveget a stdo-ra (<i>standard output</i>)</p>
<pre><code>&lt;?php
echo "Hello World!";
?></code></pre>
<hr>
<h2>Állandók</h2>
<?php
//PHP állandó megadása
const NAME_OF_CONSTANT = 'supervisor';
echo NAME_OF_CONSTANT;
//NAME_OF_CONSTANT = 'boss';//állandó értéke nem változhat
echo '<br>' . M_PI;
//define('ALLANDO_NEVE','érték');
?>
<h2>Tömbök</h2>
<?php
//Tömbök
$array = [];//$array = array();//üres tömb megadása
echo '<pre>';
var_dump($array);
echo '</pre>';
$array = [1, 2, 3];
/**
 * array(3) {
 * [0] => int(1) // [index(key)] => 'value'
 * [1] => int(2)
 * [2] => int(3)
 * }
 */
echo '<pre>';
var_dump($array);
echo '</pre>';

$array = [12, 'HGy', true, M_PI];//újra megadjuk az elemeket: redeclare, override
/*echo '<pre>' . var_dump($array) . '</pre>';
echo '<pre></pre>';
var_dump($array);*/
//változó értékének kiírása (nem primitiveket is)
echo '<pre>' . var_export($array, true) . '</pre>';

echo 'Név:' . $array[1];//Tömb egy elemének elérése
echo "<br>Azonosító: $array[0]";

//tömb bővítése 1 elemmel (automatikus indexre)
$array[] = 'Új elem 1';
echo '<pre>' . var_export($array, true) . '</pre>';

//tömb bővítése 1 elemmel (irányított indexre)
$array[100] = 'Új elem 2';
echo '<pre>' . var_export($array, true) . '</pre>';

//Tömb elemeinek száma
$elementNr = count($array);
echo "<br>A tömb elemeinek száma: $elementNr";

//tömb megadása irányított indexekkel, asszociatív index-szel
$array = [
    1 => 'érték 1',
    100 => 'érték 2',
    3 => 'új érték',
    'test' => 'value', //asszociatív index
];
echo '<pre>' . var_export($array, true) . '</pre>';

$user = [
    'id' => 12,
    'name' => 'HGy',
    'email' => 'hgy@iworshop.hu',
    'is_active' => 1,
    'confirmed' => 1,
    'time_created' => date('Y-m-d H:i:s'),
];
echo '<pre>' . var_export($user, true) . '</pre>';
echo "Felhasználó neve ". $user['name'];
echo "<br>Email: {$user["email"]}";
echo "<br>Regisztráció ideje: {$user['time_created']}";

//array bővítése automatikus indexre
$array[] = 'új teszt elem';
echo '<pre>' . var_export($array, true) . '</pre>';
?>


</body>
</html>

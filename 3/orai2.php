<?php
//tömb felépítése véletlen egész (1-100) értékekkel
$numbers = [];//ide gyűjtjük a számokat
/*
Ismétlődő programrész , ciklus (loop)
for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálat; ciklusváltozó léptetése){
    //ciklusmag
    ...
}
 */
for($i = 1; $i <= 10; $i++ ){//operátor: $i=$i+1 -> $i++; $i--
    //...
    $numbers[$i] = mt_rand(1,100);
    echo '<br>'.$i;
}

echo '<pre>'.var_export($numbers,true).'</pre>';

//tömb műveletek
$max = max($numbers);
$min = min($numbers);

echo "Max: $max , min: $min";

//tömb sorrendezése
sort($numbers);
echo '<pre>'.var_export($numbers,true).'</pre>';

$numbers = array_reverse($numbers);//fordított elemsorrend
echo '<pre>'.var_export($numbers,true).'</pre>';

//tömb (objektum) bejárása - tömb elemeinek kiolvasása
/*
foreach($array as $key => $value){
    //ciklusmag: $key és $value az aktuális értékekel elérhető
}
 */
foreach($numbers as $k => $v){
    echo "<br> Aktuális index(kulcs): $k | érték: $v";
}
foreach($numbers as $v){
    echo "<br> Aktuális érték: $v";
}

/**
 * @todo HF: gyakorlás txt ből összes (a 16,17 es feladatok legyenek a végére hagyva)
 *
 * @todo HF2: feladatgyüjtemény.pdf ből az első 5 feladat megoldása
 */

<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.

//'jóvanazúgy, csak minnél hamarabb menjünk pizzázni' megoldás

for ($i = 1; $i < 6; $i++) {
    echo 'menü';
}

//medior pro megoldás
$menu = [
    'home' => 'Kezdőlap',
    'services' => 'Szolgáltatások',
    'blog' => 'Blog',
    'contact' => 'Contact',
];//menüpontok tömbje

$output = '<nav><ul class="main-nav">';//nav és lista nyitás

foreach ($menu as $k => $v) {//menüpontok fűzése

    $output .= '<li><a href="?page=' . $k . '">' . $v . '</a></li>';

}

//nav és lista zárása
$output = $output . '</ul></nav>';//operátor: $output = $output . 'valami'; $output .= 'valami' | +=, -=, *=, /=

//kiírás 1 lépésben
echo $output;


/*2. Készítsünk programot, amely kiszámolja az első 100 darab pozitív egész
szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához
vezessünk be egy változót, amelyet a program elején kinullázunk, a
ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát
sorban az 1, 2, 3, 4, ..., 100 számokat.)*/

$sum = 0;//ide gyűjtjük az összeget

for ($i = 1; $i <= 100; $i++) {
    $sum += $i;//$sum = $sum +  $i;//hozzáadjuk a sunhoz az aktuális számot
}

echo "<br>1-100 ig a számok összege: $sum.";
/*
3. Készítsünk programot, amely kiszámolja az első 7 darab pozitív egész
szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához
vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a
ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét,
tehát sorban az 1, 2, 3, ..., 7 számokat.)*/

$mtpl = 1;

for ($i = 1; $i <= 7; $i++) {
    $mtpl *= $i;
}

echo "<br>1-7 ig a számok szorzata: $mtpl";

/*4. Készítsünk programot, amely kiszámolja az első 50 darab (2-100) páros szám
összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a
ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket
hasonlóan adjuk össze, mint az első feladatban).*/

$sum = 0;

for ($i = 1; $i <= 50; $i++) {
    $sum += $i * 2;
}
echo "<br>2-100 ig a páros számok összege: {$sum}.";

/*
5. Készítsünk programot, amely kiszámolja az első 100 darab. páratlan
szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban
vegyük a ciklusváltozó kétszeresét eggyel csökkentve - így megkapjuk a
páratlan számokat. Ezeket hasonlóan adjuk össze, mint az első
feladatban).
 */
$sum = 0;

for ($i = 1; $i <= 50; $i++) {
    $sum += $i * 2 - 1;
}
echo "<br>1-100 ig a páratlan számok összege: {$sum}.";

//2es,4es és 5ös feladat megoldása egyben
$sumAll = $sumEven = $sumOdd = 0;

for ($i = 1; $i <= 100; $i++) {
    $sumAll += $i;
    //páros esetben a sumEven értékét növeljül
    if ($i % 2 == 0) {
        $sumEven += $i;
    } else {
        $sumOdd += $i;
    }
}

echo "<br>1-100 ig az egész számok összege: {$sumAll}.";
echo "<br>1-100 ig a páros egész számok összege: {$sumEven}.";
echo "<br>1-100 ig a páratlan egész számok összege: {$sumOdd}.";

//8. Készítsünk programot, amely generál egy $n (3-9 közötti) természetes számot, majd
//kiír egymás mellé N-szer az "XO" betűket és a kiírás
//után a kurzort a következő sor elejére teszi (<br>).

$n = mt_rand(3, 9);

echo "<br>A generált érték: $n<br>";

for ($i = 1; $i <= $n; $i++) {
    echo 'XO';
}
//'után a kurzort a következő sor elejére teszi'
echo '<br>';

//univerzálisabb megoldás
$string = 'XO';//ezt akarjuk nszer kiírni
//n-t generáljuk, de most átvesszük a 108. sorból
echo str_repeat($string, $n) . '<br>';

//9
for ($i = 1; $i <= $n; $i++) {
    echo 'OX';
}
echo '<br><hr>';

//10.Egészítsük ki a programunkat úgy, hogy az előző két sort N-szer
//ismételje meg a program. (Az előző két egymás utáni ciklust tegyük bele
//egy külső ciklusba.)
for ($j = 1; $j <= $n; $j++){
    //ciklus az XO nak
    for ($i = 1; $i <= $n; $i++) {
        echo 'XO';
    }
    echo '<br>';

    //ciklus az OX nek
    for ($i = 1; $i <= $n; $i++) {
        echo 'OX';
    }
    echo '<br>';
}


//18.
//Generálj 2 két természetes számot (3-9) (M,N, különböző számok), írd ki öket ($m: $n:)
// majd rajzoljunk ki a képernyőre egy M(oszlop)xN(sor) méretű téglalapot csillag (*) jelekből
$n=mt_rand(3,9);
$m=mt_rand(3,9);
echo "m: $m, n: $n<br>";

for($i=1;$i<=$n;$i++) {
    for ($j = 1; $j <= $m; $j++) {
        echo '*';
    }
    echo '<br>';
}
echo '<hr>';
for($i=1;$i<=$n;$i++) {
    echo str_repeat('*',$m).'<br>';
}
echo '<hr>';

//21.Kérjünk be két természetes számot (M,N), majd rajzoljunk ki a
//képernyőre egy MxN méretű téglalapot csillag (*) jelekből úgy, hogy a
//téglalap belseje üres legyen.

/*
-a külső ciklus adja a sorokat, 1-n
-ha a külső ciklusváltozó 1 vagy n akkor teljes * sort kell tenni azaz m-nyi *ot
-egyébként ki kell írni 1 * ot ,utána m-2 darab space(!&nbsp;!)-t, majd ismét 1 * ot
 */

/**
 * @todo HF: (6,7) , 19 és 20. feladat + 22,23
 */

<?php
/**
 * -szöveges érték választása halmazból
 * -több ágú elágazások
 * -egyedi értékek kezelése
 */

$cases = ['Hétfő', 'Kedd', 'Szerda', 'Csütörtök', 'Péntek', 'Szombat', 'Vasárnap'];//0-6 index
echo '<pre>';
var_dump($cases);
var_dump(array_rand($cases));
echo $cases[array_rand($cases)];

$randomNumber = mt_rand(0, 6);
echo $cases[$randomNumber];

$cases = [
    'monday' => 'Hétfő',
    'tuesday' => 'Kedd',
    'wednesday' => 'Szerda',
    'thursday' => 'Csütörtök',
    'friday' => 'Péntek',
    'saturday' => 'Szombat',
    'sunday' => 'Vasárnap'
];
var_dump($cases);
var_dump(array_rand($cases));
echo $cases[array_rand($cases)];

//mivel a tömb asszociativ így nem integer indexei vannak
$randomNumber = mt_rand(0, 6);
echo $cases[$randomNumber];//hiba: Warning:  Undefined array key ...
$normalizedCases = array_values($cases);
var_dump($normalizedCases[$randomNumber]);
var_dump(array_values($cases)[$randomNumber]);
echo '</pre>';
//switch
/*
5/90 Lottójáték
4 nyerőosztály
0-1: sajnos nem nyert---
2: alap nyereményosztály
3: közepes nyeremény
4: nagy nyeremény
5: főnyeremény
 */

$matches = mt_rand(0, 5);
echo "Találatok száma: $matches<br>";

//nyeremény kiírása
/*if ($matches < 2) {
    echo 'Sajnos....';
} elseif ($matches == 2) {
    //...
    echo "kis nyeremény";
} elseif ($matches == 3) {
    //...
    echo "közepes nyeremény";
} elseif ($matches == 4) {
    //...
    echo "nagy nyeremény";
} else {
    //... csak 5 lehet
    echo "Főnyeremény! Tüzijáték meg minden";
}*/

//ehelyett kezeljünk eseteket
switch ($matches) {

    case 0:
        //echo 'valami';
    case 1:
        //...
        echo 'Sajnos ttt....';
        break;

    case 2 :
        echo "Két találat";
        break;

    case 3:
        echo 'Három találat';
        break;

    case 4:
        echo 'Négy találat';
        break;

    default:
        //ha egyik esetnek sem felel meg
        echo 'Főnyeremény';
        break;
}//endswitch
echo '<br>Vége a switchnek';

//A match
$result = match ($matches) {
    0, 1 => 'Sajnos...',
    //1 => 'Sajnos...',
    2 => 'kis nyeremény',
    3 => 'közepes nyeremény',
    4 => 'nagy nyeremény',
    default => 'Főnyeremény'
};
var_dump($result);
echo $result;

//Egyedi értékekkel rendelkező halmaz
$numbers = [];
for($i=1;$i<=5;$i++){
    $numbers[] = rand(1,10);
}
echo '<pre>'.var_export($numbers,true).'</pre>';
//csak egy(ediek) maradhat(nak)
$unique = array_unique($numbers);
echo '<pre>'.var_export($unique,true).'</pre>';

/**@todo Órai feladat: */
//Készíts egy 5(!) elemű egyedi értékekkel rendelkező halmazt melynek minden értéke egy 1-90 közötti véletlen szám (1-10 között tesztelj)

//ezután egy var_exporttal írd ki a kapott tömb értékeit emelkedő értéksorrendben

$numbers = [];
for($i=1 ; count($numbers)<5; $i++){
    array_push($numbers,rand(1,10));//$numbers[] = rand(1,10);
    $numbers = array_unique($numbers);
}
echo '<pre>'.var_export($numbers,true).'</pre>';
sort($numbers);
echo '<pre>'.var_export($numbers,true).'</pre>';

//A while ciklus

/*
while(condition[true|false]){
    //ciklusmag
}
 */
//1 példa 5 (1-10) generált érték
$numbers = [];
$i=1;//'ciklusváltozó' kezdeti értéke
while($i<=5){//belépési feltétel vizsgálata
    $numbers[] = rand(1,10);
    //'ciklusváltozó' léptetése
    $i++;
}
echo '<pre>'.var_export($numbers,true).'</pre>';

//2. példa segédváltozó nélkül

$numbers = [];
while( count($numbers) < 5 ){
    $numbers[] = rand(1,10);
}
echo '<pre>'.var_export($numbers,true).'</pre>';
//3. példa segédváltozó nélkül egyedi értékekkel

$numbers = [];
while( count($numbers) < 0 ){
    $numbers[] = rand(1,10);
    $numbers = array_unique($numbers);//ismétlődés kiszedése
}
echo '<pre>'.var_export($numbers,true).'</pre>';

/*
 a do-while ciklus (hátultesztelő ciklus)
do{
    //ciklusmag
}while(condition)
 */
$numbers = [];
do{
    $numbers[] = rand(1,10);
}while(count($numbers) < 0 );

echo '<pre>'.var_export($numbers,true).'</pre>';

/**
 * @todo HF:
 * -Készits egy 5 elemű egyedi értékekkel rendelkező halmazt emelkedő értéksorrendben (ticket)
 *
* -Készits egy 5 elemű egyedi értékekkel rendelkező halmazt emelkedő értéksorrendben (results)
 *
 * -írd ki hogy a tickets halmaz hány találatot tartalmaz a results halmazból és írd ki, lehetőség szerint a találatokat is sorold fel
 *
 * -A találatok száma alapján írd ki a nyereményosztályt
 *
@hint:
 * találatok száma: metszet
 *
 * nyereményosztály: switch vagy match
 */

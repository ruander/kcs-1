<?php
//saját (metódus) funkció készítése (funkciónak van visszatérési értéke)
/**
 * Véletlen szám generálása (lotto)
 * @param int $draws | húzások száma
 * @param int $limit | 1-limit közé
 * @return array ; | generált egyedi számokkal rendelkező tömb növekvő sorrendben
 */
function generateNumbers(int $draws = 5, int $limit = 90): array
{
    //global $valid_game_types;//az metódus idejéig legyen látható a $valid_game_types

    //var_dump($draws,$limit);//az állandó, alapból globális
    if( VALID_GAME_TYPES[$draws] !== $limit ){
        trigger_error(
            "Rossz paraméterezés!" ,
            E_USER_WARNING);
        return [];
    }

    /*//ha értelmetlen a paraméterezés:
    if ($draws > $limit) {
        trigger_error("Rossz paraméterezés", E_USER_WARNING);
        return [];
    }*/

    /*$limit = 90;
    $draws = 5;*/
    $numbers = [];

    while (count($numbers) < $draws) {
        $numbers[] = mt_rand(1, $limit);
        $numbers = array_unique($numbers);
    }
    sort($numbers);

    return $numbers;
}

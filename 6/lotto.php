<?php
//declare(strict_types=1);//erősen tipusos!!!
//saját metódusok betöltése (mintha ide lenne gépelve)
include "functions.php";//ha nincs meg warning és tovább
//require "functions.php";//ha nincs megfatal_error és állj!
//include_once "functions.php";//ha nincs meg warning és tovább
//require_once "functions.php";//ha nincs megfatal_error és állj!
//die('teszt');
//Játéktipusok megadása
const VALID_GAME_TYPES = [
    5 => 90,
    6 => 45,
    7 => 35
];

$numbers = generateNumbers('5', '90');//saját eljárás hívása
$result = generateNumbers(5, 90);
$matches = array_intersect($numbers, $result);

echo "A tippjeid: " . implode(',', $numbers);
echo "<br>A sorsolás: " . implode(',', $result);

if (count($matches) < 2) {
    echo '<br>Sajnos nem nyert!';
} else {
    echo '<h2>Gratulálunk önnek ' . count($matches) . ' találata van!</h2>';
    echo 'Az eltalált számok: ' . implode(',', $matches);
}

echo '<pre>' . var_export($numbers, true) . '</pre>';



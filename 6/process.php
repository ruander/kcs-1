<?php
echo '<pre>';

//a GET szuperglobális tömb
var_dump($_GET);
//egy elem:
//$number = $_GET['number'] ?? null;//?? nullsafe operátor | ÍGY SOHA NE NYÚLJ GEThez és POSThoz: $_GET['number'], mindig!!!! : https://www.php.net/manual/en/function.filter-input.php
$number = filter_input(INPUT_GET,'number');
var_dump($number);

//a POST szuperglobális tömb
var_dump($_POST);
$number = filter_input(INPUT_POST,'number');
var_dump($number);

//a REQUEST szuperglobális tömb a GET és a POST uniója
var_dump($_REQUEST);


<?php
//akkor dolgozzunk a POST elemekkel ha van mit ...
if( !empty($_POST) ){// operátor: ! -> nem (tagadás,negálás)
    //nem üres
    var_dump($_POST);
    $number = filter_input(INPUT_POST, 'number');
    var_dump($number);
    /**
     * @todo: HF: 3 olyan példa amihez be kell kérni 1 egész számot.
     * -mind3 példa ugyanazt a bekért számot használja.
     * -extra 1: a kiírás változóba történjen majd a body területen legyen kiírva
     * -extra 2: ha nem pozitív egész számot adtak meg, akkor álljon meg a program die('nem  + egész szám!') -al;
     */

    //...
}


?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás 'azonos' fileban</title>
</head>
<body>
-extra segitség: ... (változóba gyüjtött válaszok kiírása) [nullsafe]

<h2>POST</h2>
<form method="post">
    <label>
        <span>Adj meg egy számot</span>
        <input type="text" name="number">
    </label>
    <button>Mehet</button>
</form>
</body>
</html>

<?php
//
/*
Problem
Exists
Between
Keyboard
And
Chair
*/
$output = '';//ide gyűjtjük a kiírandó elemeket
if (!empty($_POST)) {
    //var_dump($_POST);
    //hibakezelés
    $errors = [];//id gyűjtjük a mezők hibáit [fieldName] => 'error message'
    //A mező legyen egész szám
    $a = filter_input(INPUT_POST, 'a', FILTER_VALIDATE_INT);
    if ($a === false) { //if($a != true) ... if(!$a) ... if($a == false) ugyanazt jelentik, ilyenkor 0 = "0" = false = null
        $errors['a'] = '<span class="error">Érvénytelen adat!</span>';
    }

    //B mező védve van az ellen is ha meltünne az elem a POSTból arra is hibát írjon
    $b = filter_input(INPUT_POST, 'b', FILTER_VALIDATE_INT);
    if ($b === false || $b === null) {
        $errors['b'] = '<span class="error">Érvénytelen adat!</span>';
    }


    //...ha üres maradt eddig a hibatömb, akkor minden adat jó
    if (empty($errors)) {
        //die('minden ok');
        //a és b adat biztosan(!) jó
        /**
         * @todo: órai feladat:
         * az outputot egészítsd ki a válasszal hogy A nagyobb vagy B nagyobb vagy A és B egyenlőek
         * a) -if else rendszer
         * b) -switch használatával
         * c) -match használatával
         * d) spaceship operátor használatával <=>    https://www.php.net/manual/en/language.operators.comparison.php
         * ha elkészítettél egyváltozatot, kommenteld ki blokk kommentel
         */
        /*//a)
        */
        //var_dump($a <=> $b);
        $result = [
          -1 => 'B) a nagyobb',
          0 => 'A) és B) egyenlőek',
          1 => 'A) a nagyobb'
        ];
        //var_dump($result[$a <=> $b]);
        $output .= $result[$a <=> $b];//fűzzük a kiírandó stringhez a választ, $output = $output . $result[$a <=> $b]
    }

}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Gyakorlás - űrlap - hibakezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            width: 400px;
            margin: 0 auto;
            background: #ddd;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
            padding: 5px;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: .8em;
        }
    </style>
</head>
<body>
<h1>Kisebb, nagyobb vagy egyenlő?</h1>
<?php
//echo '<pre>'.var_export($errors,true).'</pre>';

echo $output;//az összegyűjtött kiírandó elemek kiírása 1 lépésben
?>
<form method="post">
    <label>
        <span>Add meg az egyik számot (A)</span>
        <input type="text" name="a" placeholder="10" value="<?php echo filter_input(INPUT_POST, 'a'); ?>">
        <?php echo $errors['a'] ?? '';//mezőhiba kiírása ha van ?>
    </label>
    <label>
        <span>Add meg a másik számot (B)</span>
        <input type="text" name="b" placeholder="10" value="<?php echo filter_input(INPUT_POST, 'b'); ?>">
        <?php echo $errors['b'] ?? '';//mezőhiba kiírása ha van ?>
    </label>

    <button>Mehet</button>
</form>
</body>
</html>

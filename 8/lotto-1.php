<?php
//erőforrások
$limit = 90;//1-limit közé várunk egész számokat
$draws = 5;//ennyi különböző számot várunk

if (!empty($_POST)) {

    echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//hibák gyűjtő halmaza

    //név legyen min 3 karakter
    $name = filter_input(INPUT_POST, 'name');

    //nem engedélyezett html elemek eltávolítása
    $name = strip_tags($name/*,['b','i','a']*/);
    //spacek eltávolítása
    $name = trim($name);// ltrim,rtrim

    if( mb_strlen($name, 'utf-8') < 3){
        $errors['name'] = '<span class="error">Hibás adat! Minimum 3 karakter!</span>';
    }

    //email legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if(!$email){
        $errors['email'] = '<span class="error">Érvénytelen adat!</span>';
    }

    //tippek hibakezelése 1-limit közé kell esniük és egyedieknek kell lenniük
    $options = [
      'options' => [
          'min_range' => 1,
          'max_range' => $limit
      ]
    ];//szűrőopciók
    $tip1 = filter_input(INPUT_POST, 'tip-1',FILTER_VALIDATE_INT, $options);

    if(!$tip1){
        $errors['tip-1'] = '<span class="error">Érvénytelen adat!</span>';
    }
    var_dump($tip1);
    if(empty($errors)){
        die('oké');
    }

}
/*űrlap:
    név: min 3 karakter
    email: legyen látszólag rendben  RFC 822 a@b.c

    tippek (5db input mező 1-90, különböző egész számokkal)
    ----------------------------------------------------------
    -ha minden oké, írjuk ki a nevet,emailt,tippeket emelkedő sorrendben
*/

$form = '<form method="post">';//űrlap nyitása
//név mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Nameless One" value="' . filter_input(INPUT_POST, 'name') . '">';

$form .= $errors['name'] ?? '';//hiba hozzáfűzése ha van

$form .= '</label>';

//Email mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST, 'email') . '">'
        . ($errors['email'] ?? '') //hiba ha van
        .'</label>';

//Tipp mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Tipp 1<sup>*</sup></span>
            <input type="text" name="tip-1" placeholder="1" value="' . filter_input(INPUT_POST, 'tip-1') . '">'
    . ($errors['tip-1'] ?? '') //hiba ha van
    .'</label>';


$form .= '<button>szelvény feladása</button>
        </form>';
//kiírás 1 lépésben (űrlap)
echo $form;

$style = "<style>
    label {
        display:flex;
        flex-direction:column;
        margin-bottom: 1em;
    }
    .error {
        color:red;
        font-style:italic;
        font-size:0.8em;
    }
</style>";

//kiírás 1 lépésben (stílusok)
echo $style;

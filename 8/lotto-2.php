<?php
//erőforrások
$limit = 35;//1-limit közé várunk egész számokat
$draws = 7;//ennyi különböző számot várunk

if (!empty($_POST)) {

    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//hibák gyűjtő halmaza

    //név legyen min 3 karakter
    $name = filter_input(INPUT_POST, 'name');

    //nem engedélyezett html elemek eltávolítása
    $name = strip_tags($name/*,['b','i','a']*/);
    //spacek eltávolítása
    $name = trim($name);// ltrim,rtrim

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Hibás adat! Minimum 3 karakter!</span>';
    }

    //email legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen adat!</span>';
    }

    //tippek hibakezelése 1-limit közé kell esniük és egyedieknek kell lenniük
    $options = [
        'filter' => FILTER_VALIDATE_INT,
        'options' => [
            'min_range' => 1,
            'max_range' => $limit,
        ],
        'flags' => FILTER_REQUIRE_ARRAY
    ];//szűrőopciók
    //$tip1 = filter_input(INPUT_POST, 'tip-1',FILTER_VALIDATE_INT, $options);
    //$tips = filter_input(INPUT_POST, 'tips', FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    $tips = filter_input(INPUT_POST, 'tips', options: $options);//az eredeti megadott értékek halmaza
    $unique_tips = array_unique($tips);//ebben csak egyedi értékek maradnak
    /*if(!$tip1){
        $errors['tip-1'] = '<span class="error">Érvénytelen adat!</span>';
    }*/
    //var_dump('<pre>',$tips,$unique_tips);
    //bejárjuk a tips-et
    foreach($tips as $nr => $tip){
        if($tip == false){//értékhatárra és egészre renben van-e
            $errors['tips'][$nr] = '<span class="error">Érvénytelen adat!</span>';
        }elseif(!array_key_exists($nr,$unique_tips)){
            //ha rendben van értékre akkor ismétlődő-e, azaz benne van e a kulcsa az egyedi halmazban
            $errors['tips'][$nr] = '<span class="error">Már tippelted!</span>';
        }
    }

    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'tips' => $tips
        ];
        echo '<pre>'.var_export($data,true).'</pre>';
        die();
    }

}
/*űrlap:
    név: min 3 karakter
    email: legyen látszólag rendben  RFC 822 a@b.c

    tippek (5db input mező 1-90, különböző egész számokkal)
    ----------------------------------------------------------
    -ha minden oké, írjuk ki a nevet,emailt,tippeket emelkedő sorrendben
*/

$form = '<form method="post">';//űrlap nyitása
//név mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Nameless One" value="' . filter_input(INPUT_POST, 'name') . '">';

$form .= $errors['name'] ?? '';//hiba hozzáfűzése ha van

$form .= '</label>';

//Email mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST, 'email') . '">'
    . ($errors['email'] ?? '') //hiba ha van
    . '</label>';

for ($i = 1; $i <= $draws; $i++) {
//Tipp mező hozzáfüzése az űrlaphoz
    $form .= '<label>
            <span>Tipp ' . $i . '<sup>*</sup></span>
            <input type="text" name="tips[' . $i . ']" placeholder="' . $i . '" value="' . (filter_input(INPUT_POST, 'tips',options: FILTER_REQUIRE_ARRAY)[$i] ?? '') . '">'
        . ($errors['tips'][$i] ?? '') //hiba ha van
        . '</label>';
}

$form .= '<button>szelvény feladása</button>
        </form>';
//kiírás 1 lépésben (űrlap)
echo $form;

$style = "<style>
    label {
        display:flex;
        flex-direction:column;
        margin-bottom: 1em;
    }
    .error {
        color:red;
        font-style:italic;
        font-size:0.8em;
    }
</style>";

//kiírás 1 lépésben (stílusok)
echo $style;

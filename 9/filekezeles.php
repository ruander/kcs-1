<?php
$content = "Ezt akarjuk eltárolni egy file-ban...";

//átnézni: https://www.php.net/manual/en/ref.filesystem.php
//fopen,fread,fwrite,fclose,chmod
//mappakezelés: https://www.php.net/manual/en/ref.dir.php
//opendir,scandir

//file_put_contents (írás) , file_get_contents (olvasás)
//FILE tartalmának beolvasása
//A file_get contents
$fileName = 'index.php';//ezt a filet olvassuk be
//$fileName = 'https://ruander.hu';
$fileContent = file_get_contents($fileName);
//var_dump($fileContent);
echo $fileContent;

//FILE készítése
//file-t csak létező mappába lehet létrehozni
$fileName = 'test.txt';//ez legyen a file neve
$dir = 'uploads/';//ebbe a mappába szeretnénk tárolni
//var_dump(file_put_contents($dir.$fileName,$content));
//ha nem létezne a mappa, akkor készítsük el
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}
file_put_contents($dir . $fileName, $content);

///////olvassuk vissz a atartalmát
/// és írjuk ki
$fileContent = file_get_contents($dir . $fileName);
echo $fileContent;

//adat tárolása
$data = [
    'name' => 'John Doe',
    'email' => 'a@b.c',
    'tipps' => [ 1, 2, 3, 4, 5 ]
];
var_dump($data);
$fileName = 'tipps.txt';
file_put_contents($dir . $fileName, $data);//ez így nem jó mert nem kezelhető visszaolvasás után (John Doea@bcArray)

//tömb -> string átalakítások
//sorozatosítás
$serializedContent = serialize($data);
file_put_contents($dir . $fileName, $serializedContent);




$dataFromFile = file_get_contents($dir . $fileName);
//visszaalakítás
$dataFromFile = unserialize($dataFromFile);
if($data === $dataFromFile){
    echo '<br>Egyeznek serialize után';
}

/////JSON adat
$jsonContent = json_encode($data);//átalakítás stringgé
echo "<br>$jsonContent";
$fileName = 'tipps.json';
//kiírás
file_put_contents($dir . $fileName, $jsonContent);



//visszaolvasás
$dataFromFile = file_get_contents($dir . $fileName);
//visszaalakítás
$dataFromFile = json_decode($dataFromFile, true);
//var_dump($dataFromFile);
if($data === $dataFromFile){
    echo '<br>Egyeznek json után';
}

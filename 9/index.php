<?php
//erőforrások
include "settings.php";//játéktipusok

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték</title>
</head>
<body>
<h1>Lottójáték</h1>
<h2>Válassz játéktipust</h2>
<?php
$list = '<ul>';//lista nyitás
foreach ( AVAILABLE_GAME_TYPES as $draws => $limit ){
    $list .= "<li><a href=\"tickets.php?draws=$draws\">$draws/$limit játék</a></li>";//játéktipusok a tömbből
}
$list .= '</ul>';//lista zárása
echo $list;
?>

</body>
</html>

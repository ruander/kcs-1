<?php
//szelvények táblázatba listázása
$fileName = 'tickets/2022/5/13.json';
//létezzen a file
if(file_exists($fileName)){
    $tickets = json_decode( file_get_contents($fileName), true);
}else{
    die('a file nem létezik:'.$fileName);
}

//$tickets itt egy asszociativ tömb a szelvényekkel
//készíts egy fejléces táblázatot:  név |   email    | tippek
//                                 Laci | a@laci.hu  | 1,2,3,4,5
//a táblázatot egy $table változóba gyűjtsd majd egy lépésben írd ki
foreach($tickets as $ticket){
    echo '<pre>'.var_export($ticket,true).'</pre>';
}
//ha van időd extrák:

// adott játékra/hétre sorsolás (pl: 13.sorsolas) {1,2,3,4,5}
//beolvasások a táblázat 4. oszlopa: találatok száma (0-5)
//ha van találat
    //a) 5.oszlop a talált számok
    //b) a 3.oszlopban az eltalált szám vastagbetűs (<b></b>)

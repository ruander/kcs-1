<?php
//Erőforrások
include "settings.php";//játéktipusok
//var_dump($_GET);
//vegyük ki a szükséges elemeket az urlből
$draws = filter_input(INPUT_GET, 'draws', FILTER_VALIDATE_INT) ?? 5;
//segédtömb alapján meg tudjuk állapítani hogy érvényes e(benne van -e a kulcsa) és ha igen akkor milyen limit tartozik hozzá
//ha nem érvényes akkor zavarjuk vissza a választáshoz
if (!array_key_exists($draws, AVAILABLE_GAME_TYPES)) {
    //átirányítás
    header('location:index.php');
    exit();
}
//ide csak akkor jutunk el ha érvényes a draws paraméter
$limit = AVAILABLE_GAME_TYPES[$draws];
//tárolás helye a mappastruktura az adatok rendezésénél van részletezve
$dir = 'tickets/' . date('Y') . '/' . $draws . '/';
//mappa ellenőrzés/létrehozás
if (!is_dir($dir)) {
        mkdir($dir, 0755, true);
}


//post hibakezelés
if (!empty($_POST)) {

    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//hibák gyűjtő halmaza

    //név legyen min 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Hibás adat! Minimum 3 karakter!</span>';
    }

    //email legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen adat!</span>';
    }

    //tippek hibakezelése 1-limit közé kell esniük és egyedieknek kell lenniük
    $options = [
        'filter' => FILTER_VALIDATE_INT,
        'options' => [
            'min_range' => 1,
            'max_range' => $limit,
        ],
        'flags' => FILTER_REQUIRE_ARRAY
    ];//szűrőopciók

    $tips = filter_input(INPUT_POST, 'tips', options: $options);//az eredeti megadott értékek halmaza
    $unique_tips = array_unique($tips);//ebben csak egyedi értékek maradnak

    //var_dump('<pre>',$tips,$unique_tips);
    //bejárjuk a tips-et
    foreach ($tips as $nr => $tip) {
        if ($tip == false) {//értékhatárra és egészre renben van-e
            $errors['tips'][$nr] = '<span class="error">Érvénytelen adat!</span>';
        } elseif (!array_key_exists($nr, $unique_tips)) {
            //ha rendben van értékre akkor ismétlődő-e, azaz benne van e a kulcsa az egyedi halmazban
            $errors['tips'][$nr] = '<span class="error">Már tippelted!</span>';
        }
    }

    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'tips' => $tips
        ];

        //tároljuk el a szelvényeket
        //mappa: tickets/2022/{draws}/{week}.json
        $fileName = date('W') . '.json';
        $tickets = [];
        //ha létezik már a file, akkor már vannak az adott jétre tippsorok (szelvények) tehát a tickets halmazunk nem üres hanem a fileból visszaolvasott halmaz
        if(file_exists($dir.$fileName)){
            $tickets = json_decode(file_get_contents($dir.$fileName),true);
        }
        //adjuk a szelvényt a tárolandó elemekhez
        echo '<pre>' . var_export($tickets, true) . '</pre>';
        $tickets[] = $data;
        //adatok jsonbe és írjuk ki
        file_put_contents($dir.$fileName, json_encode($tickets));
        header('location:index.php');//átirányitás a játékválasztásra
        exit();
    }

}



//utána(!!!!!!!)
//php űrlap összeállítása ($form)
$form = '<form method="post">';//űrlap nyitása
//név mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Nameless One" value="' . filter_input(INPUT_POST, 'name') . '">';

$form .= $errors['name'] ?? '';//hiba hozzáfűzése ha van

$form .= '</label>';

//Email mező hozzáfüzése az űrlaphoz
$form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . filter_input(INPUT_POST, 'email') . '">'
    . ($errors['email'] ?? '') //hiba ha van
    . '</label>';

for ($i = 1; $i <= $draws; $i++) {
//Tipp mező hozzáfüzése az űrlaphoz
    $form .= '<label>
            <span>Tipp ' . $i . '<sup>*</sup></span>
            <input type="text" name="tips[' . $i . ']" placeholder="' . $i . '" value="' . (filter_input(INPUT_POST, 'tips', options: FILTER_REQUIRE_ARRAY)[$i] ?? '') . '">'
        . ($errors['tips'][$i] ?? '') //hiba ha van
        . '</label>';
}

$form .= '<button>szelvény feladása</button>
        </form>';
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték <?= $draws . '/' . $limit ?></title>
    <style>
        label {
            display: flex;
            flex-direction: column;
            margin-bottom: 1em;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<h1>Lottójáték <?= $draws . '/' . $limit ?></h1>
<h2>Adja meg tippjeit:</h2>
<!--PHP űrlap kiírása-->
<?php echo $form; ?>
</body>
</html>

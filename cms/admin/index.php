<?php
//Erőforrások
require "../config/connect.php";
/** @var $link mysqli */
require "../config/settings.php";
require "../config/functions.php";
//var_dump($_SESSION);
session_start();//Munkafolyamat indítása
$sid = session_id();//mf azonosító
$page = filter_input(INPUT_GET,'p',FILTER_VALIDATE_INT) ?? 0;//oldal paraméter url-ből
$baseURL = 'index.php?p='.$page;//URL a pageből a moduloknak
//$infoPanel = '';//mindenféle kiírandó infónak
$output = '';//a kiírásoknak
//ha létezik a "menüponthoz" modul akkor töltsük be
if(isset(ADMIN_MENU[$page]['module'])) {
    $moduleFile = MODULE_DIR.ADMIN_MENU[$page]['module'].MODULE_EXT;
    if(file_exists($moduleFile)){
        include $moduleFile;/** @var $output   | a modulból kell érkezzen */
    }else{
        //$infoPanel = 'nincs ilyen modul:'.$moduleFile;
        $output = 'nincs ilyen modul:'.$moduleFile;
    }
}
//kiléptetünk ha kell
if (filter_input(INPUT_GET, 'logout') !== null) {
    logout();
}
$auth = auth();
if (!$auth) {//ha nem volt azonosítási folyamat
    header('location:login.php');
    exit;
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CMS <?php echo '| '. ADMIN_MENU[$page]['title'] ?? ''; ?></title>
</head>
<body>
<?php
//echo '<pre>';
//var_dump($sid,$spass,$_SESSION);
$userBar = "<div>Üdvözlet <b>{$_SESSION['userdata']['name']}</b>! | <a href=\"?logout=true\">Kilépés</a> </div>";


//kiírások
//echo '<div>'.$infoPanel.'</div>';
echo $userBar;

//include 'modules/users.php';
echo adminMenuHTML();

echo $output;
?>
</body>
</html>


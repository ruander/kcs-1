<?php
//require '../config/connect.php';/** @var mysqli $link - adatbazási csatlakozás betöltése */
//require '../config/functions.php';//saját eljárások
//url paraméter alajpán kapcsolás
//Create Read Update Delete - CRUD
//mivel includeolva van atz indexbe itt nem kell erőforrás
if(!isset($link)){
    header('location:index.php');
    exit;
}
//erőforrások
$action = filter_input(INPUT_GET, 'action') ?? 'read'; //create,read,update,delete
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT) ?? false;//azonosító updatehez és deletehez
$output = '';
/** @var $page int - a modul azonosítója ami az urlekbe kell az index fileban van az erőforrások között onnan veszük át */
/** @var $baseURL string - a indexből */
//űrlap feldolgozása
if (!empty($_POST)) {

    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    $errors = [];//hibák gyűjtő halmaza

    //név legyen min 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    if (mb_strlen($name, 'utf-8') < 3) {
        $errors['name'] = '<span class="error">Hibás adat! Minimum 3 karakter!</span>';
    }

    //email legyen email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $errors['email'] = '<span class="error">Érvénytelen adat!</span>';
    } else {
        //foglalt-e?
        $qry = "SELECT id FROM users WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //var_dump($row,$id);
        if ($row !== null && (int) $row[0] !== $id) {
            $errors['email'] = '<span class="error">Már foglalt email!</span>';
        }

    }

    //Jelszavak ellenőrzése min 6 karakter és egyezzenek
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    if($action==='create' || $password !== '') {//jelszó ellenőrzés új felvitelkor és update esetében ha a jelszó 1 mezőben akár 1 karakter szerepel
        if (mb_strlen($password, 'utf-8') < 6) {
            $errors['password'] = '<span class="error">Legalább 6 karakter!</span>';
        } elseif ($password !== $repassword) {
            $errors['repassword'] = '<span class="error">A jelszavak nem egyeztek!</span>';
        } else {
            //nincs hiba a jelszó mezőn
            //jelszó elkódolása
            /* $secret_key = 'S3cR3T_k3Y!';
             for($i=1;$i<20;$i++){
                 $password = md5($password);
             }*/
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }

    /** @todo: status checkbox az űrlapra */
    $status = filter_input(INPUT_POST, 'status') ?? 0;

    if (empty($errors)) {
        //adatok rendberakása
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];
        //Adatbázisban tároljuk
        //echo '<pre>' . var_export($data, true) . '</pre>';
        //die();
        //módok szétválasztása
        if($action === 'create') {
            $data['password'] = $password;
            $data['time_created'] = date('Y-m-d H:i:s');
            /*$qry = "INSERT INTO
                `users`(
                        `name`,
                        `email`,
                        `password`,
                        `status`,
                        `time_created`
                        )
                VALUES (
                        '{$data['name']}',
                        '{$data['email']}',
                        '{$data['password']}',
                        '{$data['status']}',
                        '{$data['time_created']}'
                        )";*/
            $qry = getInsertQuery('users',$data);

        }else{
            //update
            $data['time_updated'] = date('Y-m-d H:i:s');
            if($password){
                $data['password'] = $password;
            }
            /*$updatePassword = $password !== '' ? ", password = '$password'" : '';

            $qry = "UPDATE users SET
                 `name` = '{$data['name']}',
                 email = '{$data['email']}',
                 status = '{$data['status']}',
                 time_updated = '{$data['time_updated']}'
                $updatePassword
                 WHERE id = $id LIMIT 1";*/
            $qry = getUpdateQuery('users', $id,$data);

        }
        mysqli_query($link, $qry) or die(mysqli_error($link));
        $insertedId = $id ?? mysqli_insert_id($link);//amit az auto increment adott beillesztéskor, vagy a módosított id-ja
        //átirányítás
        header('location:' . $baseURL);
        exit();

    }

}

switch ($action) {
    case 'delete':
        if ($id) {
            mysqli_query($link, "DELETE FROM users where id  = $id LIMIT 1") or die(mysqli_error($link));
        }
        //átirányítás
        header('location:' . $baseURL);
        exit();
        break;
    case 'update':
        //echo 'módosítás (űrlap kitöltve): ezt-> ' . $id;
        $qry = "SELECT name,email,status FROM users WHERE id = $id LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $rowUser = mysqli_fetch_assoc($result);
        //var_dump($rowUser);
        $formTitle = "Módosítás [--$id--]";
        $buttonTitle = 'Módosítom';
    //break;
    case 'create':
        $formTitle ??= 'Új felvitel';
        //ha create ág van legyen üres $rowUser
        $rowUser ??= [];
        //php űrlap összeállítása ($form)
        $form = '<h1>' . $formTitle . '</h1>
                <form method="post">';//űrlap nyitása
//név mező hozzáfüzése az űrlaphoz
        $form .= '<label>
            <span>Név<sup>*</sup></span>
            <input type="text" name="name" placeholder="Nameless One" value="' .getInputValue('name',$rowUser).
            '">';


        $form .= $errors['name'] ?? '';//hiba hozzáfűzése ha van

        $form .= '</label>';

//Email mező hozzáfüzése az űrlaphoz
        $form .= '<label>
            <span>Email<sup>*</sup></span>
            <input type="text" name="email" placeholder="email@cim.hu" value="' . getInputValue('email', $rowUser) . '">'
            . ($errors['email'] ?? '') //hiba ha van
            . '</label>';
//jelszó 1
        $form .= '<label>
            <span>Jelszó megadása<sup>*</sup></span>
            <input type="password" name="password" placeholder="******" value="">'
            . ($errors['password'] ?? '') //hiba ha van
            . '</label>';

//jelszó 2
        $form .= '<label>
            <span>Jelszó megadása újra<sup>*</sup></span>
            <input type="password" name="repassword" placeholder="******" value="">'
            . ($errors['repassword'] ?? '') //hiba ha van
            . '</label>';
//status
//ha kipipálva küldöd el a formot,de máshol hiba van akkor maradjon ugy (a value legyen 1, checked)
        $checked = '';
        if (filter_input(INPUT_POST, 'status')) {
            $checked = 'checked';
        }elseif( isset($rowUser['status']) && $rowUser['status'] === '1' && empty($_POST)){
            $checked = 'checked';
        }
        /*
         short if-fel: $checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';
         */
        $form .= '<label>
            <span><input type="checkbox" name="status" value="1" ' . $checked . '> Aktív?</span>
          </label>';

        $form .= '<button>' . ($buttonTitle ?? 'Felvitel') . '</button>
        </form>';
        $output .= $form;
        break;
    default:
        //kérd le az összes user (id,name,email,status) adatait
        $qry = "SELECT id,name,email,status FROM users ";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));

        // -kiegészítő feladat, minden record utolsó oszlopa legyen egy művelet oszlop 2 linkkel: módosít | töröl (href="#")
        $userList = '<a href="'.$baseURL.'&amp;action=create">Új felvitel</a>
                     <table>
                      <tr>
                       <th>id</th>
                       <th>név</th>
                       <th>email</th>
                       <th>státusz</th>
                       <th>művelet</th>
                      </tr>';
        //ciklus az adatsoroknak
        while ($row = mysqli_fetch_assoc($result)) {
            $userList .= '<tr>
                           <td>' . $row['id'] . '</td>
                           <td>' . $row['name'] . '</td>
                           <td>' . $row['email'] . '</td>
                           <td>' . ($row['status'] == 1 ? 'aktív' : 'inaktív') . '</td>
                           <td><a href="'.$baseURL.'&amp;action=update&amp;id=' . $row['id'] . '">szerkeszt</a> | <a onclick="return confirm(\'Biztosan törölni akarod?\')" href="'.$baseURL.'&amp;action=delete&amp;id=' . $row['id'] . '">töröl</a></td>
                          </tr>';
        }
        $userList .= '</table>';
        $output .= $userList;
        break;
}



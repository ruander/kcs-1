<?php
const SECRET_KEY = 'S3cR3t_K3Y!%"+';//tikosításhoz
const MODULE_DIR = 'modules/';//modulok mappája
const MODULE_EXT = '.php';//modulfileok kiterjesztése

//admin menü
const ADMIN_MENU = [
    0 => [
        'title' => 'Kezdőlap',
        'module' => 'dashboard',
        'icon' => 'fas fa-tachometer-alt'
    ],
    1 => [
        'title' => 'Oldalak',
        'module' => 'pages',
        'icon' => 'fas fa-file'
    ],
    50 => [
        'title' => 'Felhasználók',
        'module' => 'users',
        'icon' => 'fas fa-users'
    ],
];
